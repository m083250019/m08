#!/bin/bash

# Define la ruta del directorio de copia de seguridad
backup_dir="/tmp/mongodb_backups/$(date +%Y%m%d)"

# Crea el directorio de copia de seguridad si no existe
mkdir -p "$backup_dir"

# Realiza la copia de seguridad de cada instancia de MongoDB
mongodump --host mongo1:27017 --out "$backup_dir"
mongodump --host mongo2:27017 --out "$backup_dir"
mongodump --host mongo3:27017 --out "$backup_dir"

# Comprime el directorio de copia de seguridad
tar -zcvf "/tmp/mongodb_backup_$(date +%Y%m%d).tar.gz" -C "$backup_dir" .

# Elimina el directorio de copia de seguridad sin comprimir
rm -rf "$backup_dir"

